package az.techlab.controller;

import az.techlab.model.NewsModel;
import az.techlab.service.FakeData;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Slf4j
@RequiredArgsConstructor
public class ProducerController {
    private static final Logger logger =
            LoggerFactory.getLogger(ProducerController.class);

    private final KafkaTemplate<String, Object> template;
    private final FakeData fakeData;
    @Value("${topic-data.name}")
    private String topicName;


    @GetMapping("/send")
    public String hello() {
        NewsModel newsModel = fakeData.makeFakeNews();
        this.template.send(topicName, String.valueOf(newsModel.getIdentifier()), newsModel);
        logger.info("Sent: {}", newsModel);
        return "Sent to Kafka via Controller: " + newsModel;
    }
}
