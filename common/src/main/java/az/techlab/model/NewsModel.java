package az.techlab.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.ToString;

@Data
@ToString
public class NewsModel {
    private final String name;
    private final String action;
    private final Long identifier;

    public NewsModel(@JsonProperty("name") final String name,
                     @JsonProperty("action") final String action,
                     @JsonProperty("identifier") final Long identifier) {
        this.name = name;
        this.action = action;
        this.identifier = identifier;
    }
}
