package az.techlab.service;

import az.techlab.model.NewsModel;
import com.github.javafaker.Faker;
import org.springframework.stereotype.Component;

import java.util.Locale;

@Component
public class FakeData {
    public NewsModel makeFakeNews() {
        Faker faker = new Faker(new Locale("ru"));
        String name = faker.book().title();
        String action = faker.options().option("created", "edited", "deleted");
        Long id = System.currentTimeMillis();
        return new NewsModel(name, action, id);
    }
}
