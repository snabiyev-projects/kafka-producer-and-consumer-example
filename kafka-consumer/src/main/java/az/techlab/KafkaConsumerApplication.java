package az.techlab;

import az.techlab.model.NewsModel;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.common.header.Headers;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.kafka.annotation.KafkaListener;

import java.util.stream.StreamSupport;

@SpringBootApplication
@Slf4j
public class KafkaConsumerApplication {
    private static final Logger logger =
            LoggerFactory.getLogger(KafkaConsumerApplication.class);

    public static void main(String[] args) {
        SpringApplication.run(KafkaConsumerApplication.class, args);
    }


    @KafkaListener(id = "newsReader1", groupId ="news-loggers", topics = "news-topic", clientIdPrefix = "json",
            containerFactory = "kafkaListenerContainerFactory")
    public void listen(ConsumerRecord<String, NewsModel> cr) {
        logger.info("Logger 1 [JSON] received key {}: Type [{}] | Record: {}", cr.key(),
                typeIdHeader(cr.headers()), cr);
    }


    private static String typeIdHeader(Headers headers) {
        return StreamSupport.stream(headers.spliterator(), false)
                .filter(header -> header.key().equals("__TypeId__"))
                .findFirst().map(header -> new String(header.value())).orElse("N/A");
    }
}
