package az.techlab.service;

import az.techlab.model.NewsModel;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class ScheduledTask {
    private final FakeData fakeData;
    private final KafkaTemplate<String, Object> template;
    @Value("${topic-data.name}")
    private String topicName;

    @Scheduled(fixedRate = 10000)
    public void makeNews() {
        NewsModel newsModel = fakeData.makeFakeNews();
        this.template.send(topicName, String.valueOf(newsModel.getIdentifier()), newsModel);
        System.out.println("Sent to Kafka via TaskScheduler: " + newsModel);
    }
}
